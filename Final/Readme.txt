Tổng kết phân công việc nhóm:
1. Hoàn thành tài liệu SRS lần 1: Trần Bảo Ngọc, Vũ Tuấn Đạt, Vũ Nguyễn Việt Hoàng
2. Hoàn thiện tài liệu SRS: Trần Bảo Ngọc, Vũ Tuấn Đạt, Vũ Nguyễn Việt Hoàng, Mai Đình Tuấn
3. Vẽ sơ đồ chuyển đổi màn hình cho nhóm: Trần Bảo Ngọc, Vũ Tuấn Đạt, Vũ Nguyễn Việt Hoàng, Mai Đình Tuấn
4. Tạo màn hình cho cả nhóm dưới dạng fxml: Vũ Nguyễn Việt Hoàng
5. Thiết kế các subsystem: Trần Bảo Ngọc, Vũ Tuấn Đạt
6. Tổ chức package và đăt tên: Trần Bảo Ngọc
7. Vẽ biểu đồ package cho toàn nhóm: Trần Bảo Ngoc, Vũ Tuấn Đạt
8. Phân tích thiết kế và ý nghĩa: Vũ Tuấn Đạt
9. Hoàn thiện final report: Trần Bảo Ngọc, Vũ Tuấn Đạt, Vũ Nguyễn Việt Hoàng, Mai Đình Tuấn

Tỉ lệ đóng góp vào kết quả chung:
Trần Bảo Ngọc: 30%
Vũ Tuấn Đạt: 30%
Vũ Nguyễn Việt Hoàng: 25%
Mai Đình Tuấn: 15%