import java.util.Scanner;

public class Calculation {
    public static void main(String[] args) {
        Scanner c = new Scanner(System.in);
        System.out.print("nhap so thu nhat: ");
        int a = c.nextInt();
        System.out.print("nhap so thu hai: ");
        int b = c.nextInt();
        int sum = a + b;
        int minus = a - b;
        int mul = a * b;
        int dev = a / b;
        System.out.println(a + " + " + b + " = " + sum);
        System.out.println(a + " - " + b + " = " + minus);
        System.out.println(a + " * " + b + " = " + mul);
        System.out.println(a + " / " + b + " = " + dev);
        c.close();
    }
}
