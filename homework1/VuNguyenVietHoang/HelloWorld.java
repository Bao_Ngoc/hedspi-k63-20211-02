import java.util.Scanner;

public class HelloWorld{
    public static void main(String[] args) {
        System.out.println("Your name is: ");
        Scanner c = new Scanner(System.in);
        String name = c.nextLine(); 
        System.out.println("Hello " + name);
        c.close();
    }
}