
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        double num1,num2;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter first number: ");
        num1 = scanner.nextDouble();
        System.out.print("Enter second number: ");
        num2 = scanner.nextDouble();
        System.out.println("\nResult of Addition: " + (num1+num2));
        System.out.println("Result of Subtraction: " + (num1-num2));
        System.out.println("Result of Multiplication: " + (num1*num2));
        System.out.println("Result of Division: " + (num1/num2));
    }
}

