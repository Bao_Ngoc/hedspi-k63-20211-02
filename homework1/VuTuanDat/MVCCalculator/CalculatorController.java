package MVCCalculator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorController {
    private CalculatorView theView;
    private CalculatorModel theModel;

    //constructor
    public CalculatorController(CalculatorView theView, CalculatorModel theModel){
        this.theModel = theModel;
        this.theView = theView;

        // Tell the view: CalculateButton clicked --> execute actionPerformed method
        // in the CalculateListener class
        this.theView.addCalculateListener(new CalculateListener());
    }

    class CalculateListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int firstNumber, secondNumber = 0;
            try{
                firstNumber = theView.getFirstNumber();
                secondNumber = theView.getSecondNumber();
                theModel.addTwoNumbers(firstNumber,secondNumber);
                theView.setCalcSolution(theModel.getCalculateValue());
            } catch (NumberFormatException e){
                theView.displayErrorMessage(e.getMessage());
            }
        }
    }
}
