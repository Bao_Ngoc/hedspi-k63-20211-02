package MVCCalculator;

public class CalculatorMain {
    public static void main(String[] args) {
        // BACKEND PROCESS
        CalculatorModel theModel = new CalculatorModel();

        // FRONTEND PROCESS
        CalculatorView theView = new CalculatorView();

        // MAIN CLASS DEPENDS ON A CONTROLLER --> MVC
        CalculatorController theController = new CalculatorController(theView,theModel);
        theView.setVisible(true);
    }
}
