package MVCCalculator;

public class CalculatorModel {
    //Holds the value of the sum of the numbers entered in the view
    private int calculateValue;

    public void addTwoNumbers(int num1, int num2){
        calculateValue = num1 + num2;
    }

    public void subtractTwoNumbers(int num1, int num2){
        calculateValue = num1 - num2;
    }

    public int getCalculateValue(){
        return calculateValue;
    }
}
