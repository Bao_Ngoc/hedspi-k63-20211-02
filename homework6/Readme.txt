Trần Bảo Ngọc:
- package/class làm việc:
	+ package fxml_view
	+ package view/returnBike, view/bank
	+ package model: BikeDock, RentBikeHistory
	+ package controller: EcoMainController, ReturnBikePageController, PaymentController
	+ package bikeDockSubsystem
	+ package rentBikeHistorySubsystem
	+ package bankSubsystem
	+ package test/TranBaoNgocTest

Vũ Tuấn Đạt
- package/class làm việc: 
	+ package fxml_view/rentBike & fxml_view/payment
	+ package view/rentBike, view/bank 
	+ package model: GeneralBike
	+ package controller: EcoMainController, RentBikeController, PaymentController
	+ package generalBikeSubsystem
	+ package bankSubsystem 
	+ package rentBikeHistorySubsystem
	+ package test/VuTuanDatTest
	+ package bikeFactory

Mai Đình Tuấn
-package/class làm việc:
	+ package fxml_view/createBike
	+ package controller: EcoMainController, AdminController, AddBikeController
	+ package generalBikeSubsystem
	+ package test/MaiDinhTuanTest

Vũ Nguyễn Việt Hoàng
-package/class làm việc:
	+ package fxml_view/listdock/SearchListDock
	+ package fxml_view/listdock/DetailDock
	+ package controller: EcoMainController, ViewListDockController
	+ package view/listdock/ListDockPage

